﻿using System;

namespace _08_Heritage
{
    class Program
    {
        static void Main(string[] args)
        {
            VoiturePriotaire vp = new VoiturePriotaire("Ford", "fr59-3788", false);
            vp.Accelerer(10);
            Console.WriteLine(vp);
            vp.AlumerGyro();
            Console.WriteLine(vp);
            vp.EteindreGyro();
            vp.Accelerer(10);
            Console.WriteLine(vp);

            // Tostring
            Voiture v = new Voiture();
            //Console.WriteLine(v.ToString());
            Console.WriteLine(v);

            Voiture v2 = new Voiture("Opel", "fr62-05845");

            Console.WriteLine(v.Equals(v2));

            CompteEpargne ce = new CompteEpargne(5, "John Doe");
            ce.Crediter(100.0);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();

            Console.WriteLine(ce is Voiture);       // test si ce est de type voiture
            Console.WriteLine(v is Voiture);        // test si v est de type voiture

            // Polymorphisme
            //Animal a = new Animal(3, 3.5);    // Impossible la classe est abstraite
            //a.EmettreUnSon();
            //Console.WriteLine(a);

            Animal a1 = new Chien("Snoopy", 5, 4.0);    // On peut créer une instance de Chien (classe fille) qui sera référencée par une référence Animal (classe mère)
                                                        // L'objet chien sera vue comme un Animal on ne pourra pas accèder au propriété et au méthode  propre au chien Nom,...
            a1.EmettreUnSon();  // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée
            Console.WriteLine(a1);

            if (a1 is Chien)    // test si a1 est de "type" Chien
            {
                // Chien chien2 = (Chien)a1;    // Pour passer d'une super-classe à une sous-classe, il faut le faire expicitement avec un cast ou avec l'opérateur as
                Chien chien2 = a1 as Chien;     // as equivalant à un cast pour un objet
                chien2.Nom = "Rantanplan";  // avec la référence chien2 on a bien accès à toutes les propriétées de la classe chien
                Console.WriteLine(chien2);
            }

            Animal[] tab = new Animal[5];
            tab[0] = new Chien("Snoopy", 5, 4.0);
            tab[1] = new Chat("Tom", 6, 6, 2.5);
            tab[2] = new Chien("Idefix", 5, 4.0);
            tab[3] = new Chat("Garfield", 6, 6, 2.5);
            tab[4] = new Chien("Laika", 5, 4.0);

            foreach (Animal an in tab)
            {
                an.EmettreUnSon();
            }

            ICanWalk[] tabInter = new ICanWalk[5];       // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            tabInter[0] = new Chien("Snoopy", 5, 4.0);   // Le chien n'est vu que comme un interface ICanWalk, on ne peut utiliser que les méthode de ICanWalk
            tabInter[1] = new Chat("Tom", 6, 6, 2.5);
            tabInter[2] = new Canard(5, 4.0);
            tabInter[3] = new Chat("Garfield", 6, 6, 2.5);
            tabInter[4] = new Chien("Laika", 5, 4.0);

            foreach (ICanWalk icw in tabInter)
            {
                icw.Marcher();
            }

            Console.ReadKey();
        }
    }
}
