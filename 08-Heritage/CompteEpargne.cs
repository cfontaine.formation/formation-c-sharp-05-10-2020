﻿namespace _08_Heritage
{
    class CompteEpargne : CompteBancaire
    {
        double Taux { set; get; }

        public CompteEpargne(double taux, string titulaire) : base(titulaire)
        {
            Taux = taux;
        }

        public CompteEpargne(double taux, double solde, string titulaire) : base(solde, titulaire)
        {
            Taux = taux;
        }

        public void CalculInterets()
        {
            Solde *= (1 + Taux / 100);
        }
    }
}
