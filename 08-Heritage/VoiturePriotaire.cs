﻿using System;

namespace _08_Heritage
{
    public class VoiturePriotaire : Voiture
    {
        public bool Gyro { get; private set; }

        public VoiturePriotaire(string marque, string plaqueIma, bool gyro = true) : base(marque, plaqueIma, "Blanc") // base pour appeler le constructeur de la classe mère
        {
            Gyro = gyro;
        }

        public void AlumerGyro()
        {

            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        public new void Accelerer(int accVitesse)
        {
            Console.WriteLine("Accéler voiture priotaire");
            Vitesse += 2 * accVitesse; // comme vitesse à la visibilité protected on peut y accéder dans une sous-classe
        }

        public override string ToString()
        {// base pour appeler une méthode de la classe mère
            return string.Format("Voiture Prioritaire[{0},Gyro={1}]", base.ToString(), Gyro ? "gyro allumé" : "gyro éteint");
        }
    }
}
