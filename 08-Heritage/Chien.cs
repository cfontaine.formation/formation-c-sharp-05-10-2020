﻿using System;

namespace _08_Heritage
{
    class Chien : Animal, ICanWalk // La classe Chien hérite de la classe Animal et implémente l'interface ICanWalk
    {
        public string Nom { get; set; }

        public Chien(string nom, int age, double poid) : base(age, poid)
        {
            Nom = nom;
        }

        public override void EmettreUnSon()    // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        {
            Console.WriteLine("{0} aboie", Nom);
        }


        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface ICanWalk
        {
            Console.WriteLine("Le chien Marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le chien Court");       // Impléméntation de la méthode Courrir de l'interface ICanWalk
        }

        public override string ToString()
        {
            return string.Format("Chien [age= {0}, poid= {1}, Nom={2}]", Age, Poid, Nom);
        }
    }
}
