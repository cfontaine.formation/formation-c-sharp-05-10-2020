﻿using System;

namespace _08_Heritage
{
    class Chat : Animal, ICanWalk
    {
        public string Nom { get; set; }
        public int NbVie { get; private set; } = 9;

        public Chat(string nom, int nbVie, int age, double poid) : base(age, poid)
        {
            Nom = nom;
            NbVie = nbVie;
        }
        public override void EmettreUnSon()
        {
            Console.WriteLine("{0} miaule", Nom);
        }

        public override string ToString()
        {
            return string.Format("Chat [age= {0}, poid= {1}, Nom={2} NbVie={3}]", Age, Poid, Nom, NbVie);
        }

        public void Marcher()
        {
            Console.WriteLine("Le chat Marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le chat Court");
        }
    }
}
