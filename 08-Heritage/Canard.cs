﻿using System;

namespace _08_Heritage
{
    class Canard : Animal, ICanWalk, ICanFly  // On peut implémenter plusieurs d'interface
    {
        public Canard(int age, double poid) : base(age, poid)
        {
        }


        public override void EmettreUnSon()
        {
            Console.WriteLine("Coin Coin");
        }

        public void Marcher()
        {
            Console.WriteLine("Le canard Marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le canard Court");
        }

        public override string ToString()
        {
            return string.Format("Canard[age= {0}, poid= {1}]", Age, Poid);
        }

        public void decoller()
        {
            Console.WriteLine("Le canard Décolle");
        }

        public void atterir()
        {
            Console.WriteLine("Le canard Atterie");
        }
    }
}
