﻿namespace _08_Heritage
{
    // Un interface ne définit que des méthodes qui deveront être obligatoirement implémenté dans la classe qui va impléménter l'interface
    // Un interface commence toujours par convention par I
    interface ICanWalk
    {
        void Marcher();
        void Courir();
    }
}
