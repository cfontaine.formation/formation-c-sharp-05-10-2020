﻿namespace _08_Heritage
{
    abstract class Animal    // Animal est une classe abstraite, elle ne peut pas être instantiée elle même
    {                       // mais uniquement par l'intermédiaire de ses classses filles
        public int Age { get; private set; }
        public double Poid { get; set; }

        public Animal(int age, double poid)
        {
            Age = age;
            Poid = poid;
        }
        public abstract void EmettreUnSon();     // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles
        //{
        //    Console.WriteLine("L'animal émet un son");
        //}

        public override string ToString()
        {
            return string.Format("Animal[age= {0}, poid= {1}]", Age, Poid);
        }
    }
}
