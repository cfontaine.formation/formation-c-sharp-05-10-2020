﻿using System;
using System.Collections.Generic;

namespace _08_Heritage
{
    /*sealed*/
    public class Voiture    //  sealed empecher l'héritage de cette classe
    {
        protected double niveauCarburant = 10;
        public string Marque { get; set; } = "Honda";
        public string Couleur { get; set; } = "Noir";
        public string PlaqueIma { get; private set; } = "";
        public int Vitesse { get; protected set; }
        public static int CompteurVoiture { get; private set; }
        public double NiveauCarburant
        {
            get
            {
                return niveauCarburant;
            }
            set
            {
                if (value >= 0)
                {
                    niveauCarburant = value;
                }
            }
        }

        public Voiture()
        {
            Console.WriteLine("Constructeur classe Mère");
            CompteurVoiture++;
        }
        public Voiture(string marque, string plaqueIma) : this()
        {
            Marque = marque;
            PlaqueIma = plaqueIma;
        }

        public Voiture(string marque, string couleur, string plaqueIma) : this(marque, plaqueIma)
        {
            Couleur = couleur;
        }

        public void Accelerer(int accVitesse)
        {
            Console.WriteLine("Accéler voiture");
            if (accVitesse > 0)
            {
                Vitesse += accVitesse;
            }
        }
        public void Freiner(int frnVitesse)
        {
            if (frnVitesse > 0)
            {
                Vitesse -= frnVitesse;
            }
            if (Vitesse < 0)
            {
                Vitesse = 0;
            }
        }
        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }
        public bool TestEgalitePlaque(Voiture a)
        {
            return PlaqueIma == a.PlaqueIma;
        }

        public override string ToString()
        {
            return string.Format("Voiture [Marque={0} Couleur={1} Plaque={2} Vitesse={3} Caburant={4}]", Marque, Couleur, PlaqueIma, Vitesse, niveauCarburant);
        }

        public override bool Equals(object obj)
        {
            return obj is Voiture voiture &&
                   PlaqueIma == voiture.PlaqueIma;
        }

        public override int GetHashCode()
        {
            return 711479286 + EqualityComparer<string>.Default.GetHashCode(PlaqueIma);
        }

        public static bool TestEgalitePlaque(Voiture a, Voiture b)
        {
            return a.PlaqueIma == b.PlaqueIma; // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
