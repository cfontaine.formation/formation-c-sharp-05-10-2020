﻿using System;
using System.IO;

namespace _13_io
{
    class Program
    {
        static void Main(string[] args)
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drv in drives)
            {
                Console.WriteLine(drv.Name);            // Nom du lecteur
                Console.WriteLine(drv.TotalFreeSpace);  // espace disponible sur le lecteur
                Console.WriteLine(drv.DriveFormat);     // système de fichiers du lecteur NTFS, FAT ..
                Console.WriteLine(drv.TotalSize);       // espace total du lecteur
                Console.WriteLine("______________________");
            }

            //  Teste si le dossier existe
            if (!Directory.Exists(@"c:\Formations\TestIO\csharp\Test"))
            {
                Directory.CreateDirectory(@"c:\Formations\TestIO\csharp\Test"); // Création du répertoire 
            }
            string[] path = Directory.GetFiles(@"c:\Formations\TestIO\csharp\Test");    // liste les fichiers du réperotire
            //foreach (string p in path)    // efface tous les fichiers du répertoire
            //{
            //    Console.WriteLine(p);
            //    File.Delete(p);
            //}
            EcrireFichierTexte(@"c:\Formations\TestIO\csharp\test.txt");
            LireFichierTexte(@"c:\Formations\TestIO\csharp\test.txt");
            EcrireFichierBinaire(@"c:\Formations\TestIO\csharp\test.bin");
            LireFichierBinaire(@"c:\Formations\TestIO\csharp\test.bin");
            Console.ReadKey();

        }


        public static void EcrireFichierTexte(string chemin)
        {
            // Using => Équivalent d'un try / finally + Close()
            using (StreamWriter sw = new StreamWriter(chemin, true)) // StreamWriter Ecrire un fichier texte
            {                                                       // append à true "compléte" le fichier s'il existe déjà, à false le fichier est écrasé

                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello World!!!");
                }
            }
        }

        public static void LireFichierTexte(string chemin)
        {
            using (StreamReader sr = new StreamReader(chemin)) // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream) // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    Console.WriteLine(sr.ReadLine());
                }
            }
        }

        public static void EcrireFichierBinaire(string chemin)
        {
            using (FileStream fs = new FileStream(chemin, FileMode.Create))       // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte i = 0; i < 100; i++)
                {
                    fs.WriteByte(i);            // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBinaire(string chemin)
        {
            byte[] tab = new byte[100];
            using (FileStream fs = new FileStream(chemin, FileMode.Open))
            {
                //for (byte i = 0; i < 100; i++)
                //{
                //    Console.WriteLine(fs.ReadByte()); // // Lecture d'un octet dans le fichier
                //}

                fs.Read(tab, 0, 100);       // lecture de 100 octets dand le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                foreach (byte b in tab)
                {
                    Console.WriteLine(b);
                }
            }
        }


    }
}
