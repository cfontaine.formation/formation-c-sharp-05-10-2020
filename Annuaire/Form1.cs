﻿using Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Annuaire
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ContactDao.ConnectionStr= ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString;
            InitListView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textPrenom.TextLength == 0 || textBoxNom.TextLength == 0 || textBoxEmail.TextLength == 0)
            {
                MessageBox.Show("Tous les champs ne sont pas remplis", "Ajouter un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else if (!Regex.IsMatch(textBoxEmail.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
            {
                MessageBox.Show("L'email n'est pas au bon format", "Ajouter un comptact", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ContactDao dao = new ContactDao();
                Contact contact = new Contact(textPrenom.Text, textBoxNom.Text, dateTimeJnaissance.Value, textBoxEmail.Text);
                dao.SaveOrUpdate(contact);
                AddListView(contact);
                ClearTextBox();
            }
        }

        private void InitListView()
        {
            ContactDao dao = new ContactDao();
            List<Contact>lst=dao.ReadAll();
            listView1.Items.Clear();
            foreach (Contact c in lst)
            {
                AddListView(c);
            }
        }

        private void AddListView(Contact contact)
        {
            ListViewItem item1 = new ListViewItem(contact.Prenom, 0);
            item1.SubItems.Add(contact.Nom);
            item1.SubItems.Add(contact.JourNaissance.ToShortDateString());
            item1.SubItems.Add(contact.Email);
            listView1.Items.Add(item1);
        }

        private void ClearTextBox()
        {
            textBoxEmail.Text = "";
            textBoxNom.Text = "";
            textPrenom.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }
    }
}
