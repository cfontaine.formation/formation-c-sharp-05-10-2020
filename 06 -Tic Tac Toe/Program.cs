﻿using System;

namespace _06__Tic_Tac_Toe
{

    class Program
    {
        // Exercice Tic tac toe
        // - Saisir le coup de chaque joueur alternativement (ligne colonne)
        // - Tester, si la case n'a pas été jouée
        // - Stocker les coups joués dans un tableau de caractère 
        //      Espace-> case libre
        //      X-> Joueur 1
        //      O-> Joueur 2
        // - Afficher le tableau de caractère qui contient les coups joués
        // [ ][ ][ ]
        // [ ][ ][X]
        // [ ][O][ ]
        // - Tester si un joueur a gagné
        static void Main(string[] args)
        {
            char[,] grille = InitGrille(3);
            char currentPlayer = 'O';
            int nbCoup = 0;
            for (; ; )
            {
                nbCoup++;
                ChangePlayer(ref currentPlayer);
                JouerCoup(ref grille, currentPlayer);

                if (nbCoup == grille.Length)
                {
                    AfficherGrilleJeu(grille);
                    Console.WriteLine("Egalité");
                    break;
                }

                if (TestDiagonal(grille) && TestVertical(grille) && TestHorizontal(grille))
                {
                    AfficherGrilleJeu(grille);
                    Console.WriteLine("Le joueur {0} a gagné", currentPlayer);
                    break;
                }
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Création et initialisation de la grille de jeu
        /// </summary>
        /// <param name="size">Taille de la grille</param>
        /// <returns>Un tableau à 2 dimensions de taille size et initialisé avec le caractère espace</returns>
        static char[,] InitGrille(int size)
        {
            char[,] grille = new char[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    grille[i, j] = ' ';
                }
            }
            return grille;
        }

        /// <summary>
        /// Méthode qui permet de jouer un coup
        /// - Affiche la grille
        /// - Saisie un coup
        /// - Modifie la grille en conséquence
        /// </summary>
        /// <param name="grille">Tableau à 2 dimensions contenant la grille de jeu</param>
        /// <param name="joueur">Le caractère représentant le joueur</param>
        static void JouerCoup(ref char[,] grille, char joueur)
        {
            AfficherGrilleJeu(grille);
            SaisirCoup(grille, out int l, out int c);
            grille[l, c] = joueur;
        }

        /// <summary>
        /// Méthode qui permet
        /// - de saisir un coup valide
        /// - de tester s'il n'est pas en dehors de la grille
        /// - de tester si le coup n'a pas été joué
        /// </summary>
        /// <param name="grille">Tableau à 2 dimensions contenant la grille de jeu</param>
        /// <param name="l">Retourne la ligne qui correspond au coup joué</param>
        /// <param name="c">Retourne la colonne qui correspond au coup joué </param>
        static void SaisirCoup(char[,] grille, out int l, out int c)
        {
            for (; ; )
            {
                try
                {
                    Console.WriteLine("Entrer la ligne");
                    l = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Entrer la colonne");
                    c = Convert.ToInt32(Console.ReadLine());
                    if (TestCoupInvalide(grille, l, c))
                    {
                        Console.WriteLine("Coup invalide");
                    }
                    else if (TestCoupDejaJouer(grille, l, c))
                    {
                        Console.WriteLine("Coup déjà joué");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("La saisie doit être un chiffre");
                }
            }
        }

        /// <summary>
        /// Afficher une grille de jeu dans la console
        /// </summary>
        /// <param name="grille">Un tableau à 2 dimensions qui représente la grille de jeu</param>
        static void AfficherGrilleJeu(char[,] grille)
        {
            for (int l = 0; l < grille.GetLength(0); l++)
            {
                for (int c = 0; c < grille.GetLength(1); c++)
                {
                    Console.Write("[{0}] ", grille[l, c]);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Tester si un coup se trouve en dehors de la grille
        /// </summary>
        /// <param name="grille">Un tableau à 2 dimensions qui représente la grille de jeu</param>
        /// <param name="l">La ligne du coup joué</param>
        /// <param name="c">La colonne du coup joué</param>
        /// <returns>Vrai, si le coup se trouve en dehors de la grille</returns>
        static bool TestCoupInvalide(char[,] grille, int l, int c)
        {
            return l < 0 || c < 0 || l > grille.GetLength(0) || c > grille.GetLength(1);
        }

        /// <summary>
        /// Tester si un coup a déjà été joué
        /// </summary>
        /// <param name="grille">Un tableau à 2 dimensions qui représente la grille de jeu</param>
        /// <param name="l">La ligne du coup joué</param>
        /// <param name="c">>La colonne du coup joué</param>
        /// <returns>Vrai, si le est éjà joué</returns>
        static bool TestCoupDejaJouer(char[,] grille, int l, int c)
        {
            return grille[l, c] != ' ';
        }

        /// <summary>
        /// Permer de passer d'un joueur à l'autre
        /// </summary>
        /// <param name="c">Caractère représentant le jouer</param>
        static void ChangePlayer(ref char c)
        {
            c = c == 'O' ? 'X' : 'O';
        }

        /// <summary>
        /// Teste si un joueur à gagner sur les lignes horizontales
        /// </summary>
        /// <param name="grille">Tableau à 2 dimmensions contenant la grille de jeu</param>
        /// <returns>Vrai, si un joueur a gagné</returns>
        static bool TestHorizontal(char[,] grille)
        {
            for (int i = 0; i < grille.GetLength(0); i++)
            {
                char c = grille[i, 0];
                bool test = c != ' ';
                for (int j = 1; j < grille.GetLength(1); j++)
                {
                    test &= (c == grille[i, j]);
                }
                if (test)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Teste si un joueur à gagner sur les lignes verticales
        /// </summary>
        /// <param name="grille">Tableau à 2 dimmensions contenant la grille de jeu</param>
        /// <returns>Vrai, si un joueur a gagné</returns>
        static bool TestVertical(char[,] grille)
        {
            for (int i = 0; i < grille.GetLength(0); i++)
            {
                char c = grille[0, i];
                bool test = c != ' ';
                for (int j = 1; j < grille.GetLength(1); j++)
                {
                    test &= (c == grille[j, i]);
                }
                if (test)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Teste si un joueur à gagner sur les diagonales
        /// </summary>
        /// <param name="grille">Tableau à 2 dimmensions contenant la grille de jeu</param>
        /// <returns>Vrai, si un joueur a gagné</returns>
        static bool TestDiagonal(char[,] grille)
        {
            int size = grille.GetLength(0) - 1;
            char c1 = grille[0, 0];
            bool t1 = c1 != ' ';
            char c2 = grille[0, size];
            bool t2 = c2 != ' ';
            for (int i = 0; i < size + 1; i++)
            {
                t1 &= grille[i, i] == c1;
                t2 &= grille[i, size - i] == c2;
            }
            return t1 || t2;
        }

    }
}
