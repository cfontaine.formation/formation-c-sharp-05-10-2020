﻿using System;
using System.Net;

namespace _10_SurchargeOperateur
{
    class Fraction
    {
        public int Numerateur { get; set; }
        public int Denominateur { get; set; } = 1;

        public Fraction(int numerateur, int denominateur)
        {
            Numerateur = numerateur;
            if (denominateur == 0)
            {
                throw new ArithmeticException("Denominateur égale à 0");
            }
            Denominateur = denominateur;
            
        }

        public double calculer()
        {
            return ((double)Numerateur) / Denominateur;
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            int n = f1.Numerateur * f2.Denominateur + f2.Numerateur * f1.Denominateur;
            int d = f1.Denominateur * f2.Denominateur;
            int p = pgcd(n,d);
            return new Fraction(n / p, d / p);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            int n = f1.Numerateur * f2.Numerateur;
            int d = f1.Denominateur * f2.Denominateur;
            int p = pgcd(n, d);
            return new Fraction(n / p, d / p);
        }

        public static Fraction operator *(Fraction f, int scal)
        {
            int num = f.Numerateur * scal;
            int p = pgcd(num, f.Denominateur);
            return new Fraction(num / p, f.Denominateur / p);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            return f1.Numerateur == f2.Numerateur && f1.Denominateur == f2.Denominateur;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction fraction &&
                   Numerateur == fraction.Numerateur &&
                   Denominateur == fraction.Denominateur;
        }

        public override int GetHashCode()
        {
            int hashCode = 1421187769;
            hashCode = hashCode * -1521134295 + Numerateur.GetHashCode();
            hashCode = hashCode * -1521134295 + Denominateur.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            if (Denominateur == 1)
            {
                return string.Format("{0}", Numerateur);
            }
            else
            {
                return string.Format("{0}/{1}", Numerateur, Denominateur);
            }
        }

        private static int pgcd(int a, int b)
        {
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }
            }
            return a;
        }

    }
}
