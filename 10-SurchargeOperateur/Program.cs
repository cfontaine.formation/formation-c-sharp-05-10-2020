﻿using System;

namespace _10_SurchargeOperateur
{
    class Program
    {
        static void Main()
        {
            Point a = new Point(1, 0);
            Point b = new Point(2, 2);
            Point c = (a + b) * 4;
            Console.WriteLine(c);

            Point d = new Point(1, 0);
            Console.WriteLine(a == b);
            Console.WriteLine(a == d);
            c += d; // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé

            Console.WriteLine(c);
            d = -c;
            Console.WriteLine(d);

            // Exercice Fraction
            Fraction f1 = new Fraction(1, 2);
            Fraction f2 = new Fraction(1, 2);
            Console.WriteLine(f1 + f2);
            Console.WriteLine(f1 * f2);
            Console.WriteLine(f1 * 2);
            Console.WriteLine(f1 == f2);

            Console.ReadKey();
        }
    }
}
