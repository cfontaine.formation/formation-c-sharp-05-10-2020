﻿using Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testbdd
{
    class Program
    {
        static void Main(string[] args)
        {
            // Récupération de la chaine de connection dans le fichier App.config élément <connectionStrings>
            // la chaine de connection est composée de propriété/valeur séparée par des ; 
            // Elle contient toutes les informations pour la connection à la base de donnée
            // Server-> adrewse du serveur de bdd, Port port de serveur de bdd,Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée, Pwd -> mot de passe de la bdd
            ContactDao.ConnectionStr = ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString;
            ContactDao dao = new Database.ContactDao();
            Contact c = new Contact("John", "Doe", DateTime.Now, "jd@dawan.fr");
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c,true); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}",c);
            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.ReadAll(true);
            foreach(Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}",id);
            Contact cr = dao.Read(id,true);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.JourNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr,true);
            cr = dao.Read(cr.Id, true);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr,true);
            lst = dao.ReadAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            Console.ReadKey();
        }
    }
}
