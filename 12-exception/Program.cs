﻿using System;
using System.IO;

namespace _12_exception
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = { 1, 2, 4, 5 };
            try
            {
                int val = Convert.ToInt32("azerty");    // Génére une Exception FormatException  
                tab[10] = 10;                           // Génére une Exception IndexOutOfRangeException                           
                File.OpenRead("nexistepas");            // Génére une Exception FileNotFoundException 
                Console.WriteLine("Suite des instructions");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);   // Message => récupérer le messsage de l'exception
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e) // Attrape tous les autres Exception
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Bloc finally");  // Le bloc finally est toujours éxécuter 
            }
            try
            {
                GestionAge();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);    // InnerException => exception qui est à l'origine de cette exception
                }
            }
            Console.WriteLine("Suite du Programme");
            Console.ReadKey();
        }

        static void GestionAge()
        {
            try
            {
                int age = SaisieAge();
                Console.WriteLine(age);
            }// Traitement partiel  de l'exception AgeException 
            catch (AgeException e) when (e.Age > -100) // when : le catch est éxécuté si la condition dans whene st vrai
            {
                Console.WriteLine("Traitement local {0}", e.Message);
                throw;     // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
            }
            catch (AgeException e) when (e.Age < -100)
            {
                Console.WriteLine("Traitement local", e.Message);
                throw new Exception("Erreur age", e);   // On relance une autre exception ,e =>référence à l'exception interne qui a provoquer l'exception
            }

        }
        static int SaisieAge()
        {
            int age = Convert.ToInt32(Console.ReadLine());
            if (age < 0)
            {
                throw new AgeException(age, "Age négatif");  //  throw => Lancer un exception
            }
            return 0;
        }
    }
}
