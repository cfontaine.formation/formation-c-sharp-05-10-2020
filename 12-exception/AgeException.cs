﻿using System;

namespace _12_exception
{
    // On peut créer ses propres exceptions en héritant de la classe Exception
    // Par convention, toutes les sous-classes ont un nom se terminant par Exception
    class AgeException : Exception
    {
        public int Age { get; private set; }
        public AgeException(int age) : base()     // base => appel au constructeur de la classe Exception
        {
            Age = age;
        }

        public AgeException(int age, string message) : base(message)
        {
            Age = age;
        }

        public AgeException(int age, string message, Exception inner) : base(message, inner)
        {
            Age = age;
        }
    }
}
