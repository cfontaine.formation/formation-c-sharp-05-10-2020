﻿using System;

namespace _02_SyntaxeBase
{
    // Structure
    struct Point
    {
        public int X;
        public int Y;
    }

    class Program
    {
        // Une enumération est un ensemble de constante
        enum Direction { NORD = 90, OUEST = 180, SUD = 270, EST = 0 };

        // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
        //enum Direction : short { NORD = 90, SUD = 270, EST = 0, OUEST = 180 }

        // Énumération comme indicateurs binaires
        // Pour éviter toute ambiguïté, il faut assigner explicitement tous les membres
        enum Jour_Semaine
        {
            LUNDI = 1,
            MARDI = 2,
            MERCREDI = 4,
            JEUDI = 8,
            VENDREDI = 16,
            SAMEDI = 32,
            DIMANCHE = 64,
            WEEKEND = SAMEDI | DIMANCHE // On peut spécifier des  combinaisons de membres pendant la déclaration
        }
        static void Main(string[] args)
        {
            #region Variable
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable 
            double hauteur, largeur;
            hauteur = 12.0;
            largeur = 45.5;
            Console.WriteLine(hauteur + " " + largeur);    // Concaténation avec +

            // Déclaration et initialisation de variable    type nomVariable=valeur;
            int j = 42;
            Console.WriteLine(j);

            // littéral caractère
            char ch = 'a';
            char chutf = '\u0045';
            char chhexautf = '\x45';
            Console.WriteLine(ch + " " + chutf + " " + chhexautf);

            // Littéral booléen
            bool tst = false; // ou true
            Console.WriteLine(tst);

            // Littéral entier -> int par défaut
            long l = 123L;      // L -> Litéral long
            uint ui = 123U;     // U -> Litéral unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral nombre à virgule flottante -> double par défaut
            float f = 123.5F;       // Litéral float
            Decimal deci = 123.7M;  // Litéral decimal
            Console.WriteLine(f + " " + deci);

            // Littéral entier -> chagement de base
            int dec = 42;           // décimal (base 10) par défaut
            int hexa = 0xF4;        // 0x -> héxadecimal 
            int bin = 0b100101010;  // 0b -> binaire
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Sépateur _
            int sep = 1_000_000;
            // double sd = _100_._00_;  // pas de _ en début, en fin , avant et après la virgule 
            Console.WriteLine(sep);

            // Type implicite -> var
            var v = 13.78;
            Console.WriteLine(v);
            #endregion

            #region Convertion
            // Transtypage implicite ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            double tid = tii;
            Console.WriteLine(tii + " " + tid);
            long til = tii;
            Console.WriteLine(tii + " " + til);

            // Perte de precision long -> float
            long lp1 = 123456789;
            long lp2 = 123456788;
            float fp1 = lp1;
            float fp2 = lp2;
            Console.WriteLine((lp1 - lp2) + " " + (fp1 - fp2)); // 1  et  8

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.3;
            int tei = (int)ted;
            Console.WriteLine(tei + " " + ted);

            // Dépassement de capacité
            int dpi = 300;                  // 00000001 00101100    300
            sbyte dpb = (sbyte)dpi;         //          00101100    44
            Console.WriteLine(dpi + " " + dpb);

            //Checked / Unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            int che = int.MaxValue;     // int.MaxValue => valeur maximal que peut contenir un entier
            checked
            {
                Console.WriteLine(che + 1);  //  avec checked une exception est générée s'il y a un dépassement de capacité
            }
            unchecked // (Par défaut)
            {
                Console.WriteLine(che + 1); // Plus de vérification de dépassement de capacité
            }

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir  un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérque
            Console.WriteLine("Entrer un nombre entier");
            string strConv = Console.ReadLine();
            int iConv = Convert.ToInt32(strConv);
            Console.WriteLine(iConv);

            iConv = Int32.Parse("123");
            Console.WriteLine(iConv);
            Console.WriteLine(Int32.TryParse("azerty", out iConv));
            Console.WriteLine(Int32.TryParse("345", out iConv));
            Console.WriteLine(iConv);
            #endregion

            // Type référence
            string str1 = "hello";
            string str2 = null;
            Console.WriteLine(str1 + " " + str2);
            str2 = str1;
            Console.WriteLine(str1 + " " + str2);

            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? nd = null;
            Console.WriteLine(nd.HasValue); // La propriété HasValue retourne true si nd contient une valeur (!= null) 
            nd = 12.3;
            Console.WriteLine(nd.HasValue);
            Console.WriteLine(nd.Value);    // Pour récupérer la valeur on peut utiliser la propriété Value
            Console.WriteLine((double)nd);  // Ou faire un cast 

            // Constante
            const double pi = 3.14;
            Console.WriteLine(pi);
            //pi = 1.0 ;     // Erreur: on ne peut pas modifier la valeur d'une constante

            #region Opérateur
            // Opérateur arithméthique
            int oa1 = 123;
            int oa2 = 34;
            int res = oa1 + oa2;
            Console.WriteLine(res);
            Console.WriteLine(3 % 2);  //% => modulo(reste de division entière)

            // Opérateur d'incrémentation (idem pour la décrémentation)
            // Pre-Incrementation
            int inc = 0;
            res = ++inc; // inc=1 et res=1
            Console.WriteLine(res + " " + inc);

            // Post-incrementation
            inc = 0;
            res = inc++; // inc=1 et res=0
            Console.WriteLine(res + " " + inc);

            // Opérateur d'affectation
            int ac = 45;    // Affectation
            // Affectation combinée
            ac += 23; // ac=ac+23;
            Console.WriteLine(ac);

            // Opérateur de comparaison
            bool comp = ac == 68;   // Une comparaison a pour résultat un booléen
            Console.WriteLine(comp);
            bool comp2 = ac < 10;
            Console.WriteLine(comp2);

            // Opérateur court-circuit && et || 
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            int test = 20;
            bool compEt = test > 100 && test < 10;  // comme test > 100 est faux,  test< 10 n'est pas évalué
            bool compOu = test < 100 || test > 10;  // comme test > 100 est vrai,  test>10 n'est pas évalué
            Console.WriteLine(compEt + " " + compOu);

            // Opérateur Binaires (bit à bit)
            byte bin1 = 0b01110;
            Console.WriteLine(~bin1);           // 11110001 Opérateur ~ => complémént: 1 -> 0 et 0 -> 1
            Console.WriteLine(bin1 & 0b01011);  // 01010    Et
            Console.WriteLine(bin1 | 0b01011);  // 01111    Ou          
            Console.WriteLine(bin1 ^ 0b01011);  // 00101    Ou exclusif
            Console.WriteLine(bin1 >> 1);       // 00111    Décalage à droite de 1
            Console.WriteLine(bin1 << 1);       // 11100    Décalage à droite de 1
            Console.WriteLine(4 << 4);          // 11100    Décalage à droite de 1
            Console.WriteLine(4 << 4);    //100   100000	Décalage à gauche de 4 => 64

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null ; sinon, il évalue l’opérande de droite et retourne son résultat.
            string fstr = null;
            string rstr = fstr ?? "Bonjour"; // Opérateur ?? :si fstr est null rstr a pour valeur "Bonjour" sinon fstr
            Console.WriteLine(rstr);
            fstr = "Hello";
            rstr = fstr ?? "Bonjour";
            Console.WriteLine(rstr);

            // Litérale chaine de caractères ->""
            string s = "Hello";
            Console.WriteLine(s);
            //  Format pour les chaines de caractères
            int xi = 0;
            int yi = 3;
            Console.WriteLine(string.Format("x={0}, y={1}", xi, yi));
            Console.WriteLine("x={0}, y={1}", xi, yi);    // on peut définir directement le format dans la mèthode WriteLine 
            Console.WriteLine($"x={xi}, y={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers)  
            Console.WriteLine(@"c:\temp");
            #endregion

            // Exercice: Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // - Affiche Bonjour, complété du nom saisie
            Console.WriteLine("Entrer votre nom ");
            string strEx = Console.ReadLine();
            Console.WriteLine("Bonjour, {0}", strEx);

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            int aEx = Convert.ToInt32(Console.ReadLine());
            int bEx = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("{0} + {1} = {2}", aEx, bEx, aEx + bEx);

            // Exercice: Moyenne
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            int aExMoy = Convert.ToInt32(Console.ReadLine());
            int bExMoy = Convert.ToInt32(Console.ReadLine());
            double moyenne = ((double)(aExMoy + bExMoy)) / 2;
            Console.WriteLine("Moyenne = {0}", moyenne);

            // Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.NORD;
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            Console.WriteLine(dir.ToString());
            Console.WriteLine((int)dir);

            // int-> Direction
            dir = (Direction)180;
            Console.WriteLine(dir.ToString());

            // string -> Direction
            // La méthode Parse permet de convertir une chaine de caractère n en une constante enummérée
            dir = (Direction)Enum.Parse(typeof(Direction), "SUD");
            Console.WriteLine(dir.ToString());

            // Énumération comme indicateurs binaires
            Jour_Semaine jours = Jour_Semaine.LUNDI | Jour_Semaine.MARDI;
            Console.WriteLine((jours & Jour_Semaine.MARDI) != 0);     // teste la présence de MARDI => true
            Console.WriteLine((jours & Jour_Semaine.SAMEDI) != 0);    // teste la présence de SAMEDI => false
            jours = Jour_Semaine.SAMEDI;
            Console.WriteLine((jours & Jour_Semaine.WEEKEND) != 0);   //  teste la présence de SAMEDI ou DIMANCHE => true

            // Structure
            Point p1;
            p1.X = 12;  // l'opérateur . permet d'accèder à un champs de la structure
            p1.Y = 56;
            Console.WriteLine("X={0} Y={1}", p1.X, p1.Y);
            Point p2 = p1;
            Console.WriteLine("X={0} Y={1}", p2.X, p2.Y);

            // Promotion numérique
            byte pnB = 12;
            int pnRes = -pnB; // avec un opérateur unaire, un byte est promu en int
            Console.WriteLine("{0} {1}", pnB, pnRes);

            uint pnUi = 123;
            long pnResL = -pnUi; // avec un opérateur unaire, un uint est promu en long
            Console.WriteLine("{0} {1}", pnUi, pnResL);

            long pnL = 100L;
            int pnI = 23;
            pnResL = pnL + pnI;    // pnI est promu long =>>= Le type le + petit est promu vers le + grand type des deux 
            Console.WriteLine("{0} {1} {2}", pnL, pnI, pnResL);

            // double promD = 3.0;
            // decimal promDec = 23.0M;
            // decimal promRes3 = promD + promDec;  // Erreur: promD ne peut pas être en decimal

            byte pnB1 = 34;
            byte pnB2 = 12;
            pnRes = pnB1 + pnB2;  // avec un opérateur binaire, pnB1 et pnB2 sont promus en int
            Console.WriteLine("{0} {1} {2}", pnB1, pnB2, pnRes);

            Console.ReadKey();
        }
    }
}
