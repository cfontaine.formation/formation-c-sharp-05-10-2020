﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Alias
using ConsoleSys = System.Console;
namespace _09_EspaceDeNom
{
    class Class1
    {
        public int Data { get; set; }

        public Class1(int data)
        {
            Data = data;
        }

        public void Traitement()
        {
            Console c = new Console();
            ConsoleSys.WriteLine("Traitement");
        }
    }

}
