﻿using System;

namespace _04_tableau
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // Tableau initialiser pour les entiers, les nombres à virgules flottantes =0
            //                      pour les booléen = false
            //                      pour les caractères = '\u0000'
            //                      pour les référence = null

            double[] tab = new double[3];    // Déclaration d'un tableau

            Console.WriteLine(tab[1]);      // accès au deuxième élément du tableau
            tab[1] = 3.8;
            Console.WriteLine(tab[1]);

            Console.WriteLine("Taille du tableau={0}", tab.Length);     // Length=>Nombre d'élément du tableau

            // Parcourir un tableau
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine("Tab[{0}]={1}", i, tab[i]);
            }

            // Parcourir complétement un tableau
            foreach (var v in tab)
            {
                Console.WriteLine(v);
            }

            // Déclarer un tableau en l'initialisant
            string[] tabStr = { "Hello", "World", "Bonjour" };

            for (int i = 0; i < tabStr.Length; i++)
            {
                Console.WriteLine("TabStr[{0}]={1}", i, tabStr[i]);
            }
            #endregion

            #region Exercice: Tableau
            // Trouver la valeur maximale d’un tableau de 5 entier: -7,4,8,0,-3
            // Puis la moyenne
            // Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            // int[] t = { -7, 4, 8, 0, -3 };
            Console.Write("Saisir la taille du tableau ");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] t = new int[size];
            for (int i = 0; i < t.Length; i++)
            {
                Console.Write("t[{0}]=", i);
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            int maximum = int.MinValue;
            double somme = 0;
            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] > maximum)
                {
                    maximum = t[i];
                }
                somme += t[i];
            }
            Console.WriteLine("Maximum={0} Moyenne={1}", maximum, somme / t.Length);
            #endregion

            #region  Tableau Multidimmension
            int[,,] tabMulti = new int[2, 3, 2];        // Déclaration d'un tableau à 3 dimensions

            tabMulti[0, 1, 1] = 34;                     // Accès à un élémént d'un tableau à 3 dimensions
            Console.WriteLine(tabMulti[0, 1, 1]);

            Console.WriteLine("Nombre d'éléments={0}", tabMulti.Length);       // Nombre d'élément du tableau
            Console.WriteLine("Nombre de dimmension={0}", tabMulti.Rank);      // Nombre de dimmension du tableau
            for (int i = 0; i < tabMulti.Rank; i++)
            {
                Console.WriteLine("Taille pour la dimmension {0}=", i, tabMulti.GetLength(i)); // Nombre d'élément pour la dimension
            }

            // Parcourir un tableau à 3 dimensions
            for (int i = 0; i < tabMulti.GetLength(0); i++)
            {
                for (int j = 0; j < tabMulti.GetLength(1); j++)
                {
                    for (int k = 0; k < tabMulti.GetLength(2); k++)
                    {
                        Console.WriteLine("tabMulti[{0},{1},{2}]={3}", i, j, k, tabMulti[i, j, k]);
                    }
                }
            }

            // Parcourir complétement un tableau à 3 dimensions
            foreach (int v in tabMulti)
            {
                Console.WriteLine(v);
            }
            #endregion

            // Tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEscalier = new int[3][];
            tabEscalier[0] = new int[3];
            tabEscalier[1] = new int[2];
            tabEscalier[2] = new int[4];

            tabEscalier[1][1] = 234;       // accès à un élément
            Console.WriteLine(tabEscalier[1][1]);

            Console.WriteLine("Nombre de ligne={0}", tabEscalier.Length);   // Nombre de Ligne
            for (int i = 0; i < tabEscalier.Length; i++)
            {
                Console.WriteLine("Taille de la ligne {0}={1}", i, tabEscalier[i].Length);  // Nombre de colonne pour chaque ligne
            }

            // Parcourir un tableau de tableau
            for (int i = 0; i < tabEscalier.Length; i++)
            {
                for (int j = 0; j < tabEscalier[i].Length; j++)
                {
                    Console.WriteLine("tabEscalier[{0},{1}]={2}", i, j, tabEscalier[i][j]);
                }
            }

            //  Parcourir complétement un tableau de tableau
            foreach (int[] tv in tabEscalier)
            {
                foreach (int v in tv)
                {
                    Console.WriteLine(v);
                }
            }

            Console.ReadKey();
        }
    }
}
