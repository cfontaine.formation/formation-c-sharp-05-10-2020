﻿using System;
using System.Text.RegularExpressions;

namespace _11_delegate
{
    class Program
    {
        // Déléguation => Définition de prototypes de fonctions
        public delegate int Operation(int n1, int n2);
        public delegate bool Comparaison(int n1, int n2);

        // Méthodes correspondants au prototype Operation
        // Retourne un entier et a pour paramètre 2 entiers
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }
        static void Main(string[] args)
        {   // Appel d'un délégué C#
            AfficherCalcul(1, 4, new Operation(Ajouter));
            AfficherCalcul(1, 4, new Operation(Multiplier));

            // Appel d'un délégué C# 2.0 => plus besoin de méthode correspondant au prototype
            AfficherCalcul(1, 4, delegate (int a, int b) { return a + b; });
            AfficherCalcul(1, 4, delegate (int a, int b) { return a * b; });

            // Appel d'un délégué C# 3.0 => utilisation des lambdas

            // Lambda =>méthode sans nom, les paramètres et le corps de la méthode sont séparés par l'opérateur =>
            // Syntaxe
            // () => expression     S'il n'y a pas de paramètre
            // 1 paramètre => expression
            // (paramètres) => expression
            // (paramètres) => { instructions }*
            AfficherCalcul(1, 4, (a, b) => a + b);
            AfficherCalcul(1, 4, (a, b) => a * b);


            int[] tab = { 10, -4, 7, 8, 12, -5, 3 };
            SortTab(tab, (n1, n2) => n1 < n2); // décroissant
            foreach (int t in tab)
            {
                Console.WriteLine(t);
            }
            SortTab(tab, (n1, n2) => n1 > n2); //croissant
            foreach (int t in tab)
            {
                Console.WriteLine(t);
            }

            // Expressions Régulières
            Regex reg = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Console.WriteLine(reg.IsMatch("azerty"));
            Console.WriteLine(reg.IsMatch("jd@dawan.fr"));
            Console.WriteLine(Regex.IsMatch("jd@dawan.fr", @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"));
            Console.ReadKey();
        }

        // On utilise les délégués pour passer une méthode en paramètre à une autre méthode
        static void AfficherCalcul(int a, int b, Operation op)
        {
            Console.WriteLine(op(a, b));
        }

        static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

    }
}
