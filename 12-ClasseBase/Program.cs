﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace _12_ClasseBase
{
    class Program
    {
        static void Main(string[] args)
        {
            // Chaine de caractère
            string str = "Hello world";
            string str2 = "Bonjour";
            string str3 = "Bonjour";
            Console.WriteLine(str.Length);  // Length ->Nombre de caractère de la chaine de caractère

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabètique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo(str2));
            Console.WriteLine(string.Compare(str2, str));
            Console.WriteLine(str2.Equals(str3));

            // Comparaison
            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            if ("azerty".Equals(str))
            {
                Console.WriteLine("chaine égale");
            }
            Console.WriteLine(str2 == str3);


            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(string.Concat(str, "aaa", "111"));
            Console.WriteLine(str + " azerty");
            // La méthode Join concatène les chaines  en les séparants par un caractère de séparation
            Console.WriteLine(string.Join(",", str, str2, "azerty"));

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string str4 = "azerty,qsdfg,wxcv";
            string[] tabStr = str4.Split(',');
            foreach (string s in tabStr)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine ou pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Substring(6));
            Console.WriteLine(str.Substring(6, 2));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str.Insert(5, "----"));
            // Les caractères sont supprimés à partir de l'indice pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Remove(5, 1));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str.StartsWith("Hello"));
            Console.WriteLine(str.StartsWith("Bonjour"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str.IndexOf('o'));
            Console.WriteLine(str.IndexOf('o', 5));  // idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str.IndexOf('o', 8));

            // Remplace toutes les chaines (ou caratère) oldValue par newValue 
            Console.WriteLine(str.Replace('o', 'a'));

            // Retourne True si la chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str.Contains("wo"));
            Console.WriteLine(str.Contains("jour"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str.PadLeft(50, '_'));
            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str.PadRight(50, '_'));
            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".Trim());
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".TrimStart()); // idem uniquement en début de chaine
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".TrimEnd()); // idem uniquement en fin de chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str.ToUpper());

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".Trim().ToUpper().Substring(10));


            StringBuilder sb = new StringBuilder();
            sb.Append("AZERTY");
            sb.Append("UIOP");
            string strC = sb.ToString();
            Console.WriteLine(strC);

            DateTime d = DateTime.Now;           // DateTime.Now => récupérer l'heure et la date courante
            DateTime fin2020 = new DateTime(2020, 12, 31);
            Console.WriteLine(d);
            Console.WriteLine(fin2020);
            Console.WriteLine(fin2020 - d);

            TimeSpan tsp = new TimeSpan(10, 0, 0, 0);   // TimeSpan => représente une durée
            Console.WriteLine(d.Add(tsp));
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());

            Console.WriteLine(d.Hour);
            Console.WriteLine(d.Date);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.Ticks);

            Console.WriteLine(DateTime.Parse("1955/01/23"));
            Console.WriteLine(d.ToString("dd-MMMM-yy"));

            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("Azerty");
            lst.Add(1);
            lst.Add('c');
            Console.WriteLine(lst.Count);
            Console.WriteLine(lst[0]);
            object obj = lst[0];
            if (obj is string)    // is permet de tester le type de l'objet
            {
                string strConv = obj as string; // as equivaut à un cast pour les objets
            }

            // Collection fortement typée => type générique
            List<int> lst2 = new List<int>();
            lst2.Add(12);
            lst2.Add(156);
            lst2.Add(1);
            Console.WriteLine(lst2[2]); // Accès à un élément de la liste

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(123, "Hello");    // Add => ajout d'un valeur associé à une clé
            Console.WriteLine(m[123]);  // accès à un élément m[clé] => valeur
            m.Add(23, "World");
            m.Add(2, "Bonjour");
            m[2] = "Octobre";
            m.Remove(23);

            // Parcourir un dictionnary
            foreach (KeyValuePair<int, string> kv in m)
            {
                Console.WriteLine("key={0} value={1}", kv.Key, kv.Value);
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator it = lst2.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Type généric
            Calcul<string> c1 = new Calcul<string>("azerty", "hello");
            c1.Afficher();
            Calcul<int> c2 = new Calcul<int>(2, 2);
            c2.Afficher();
            // c2.Test<double>(12.0);
            c2.Test(12.0); // Le type est déduit du type du paramètre

            // Exercice Chaine de caractères
            Console.Write("Saisir une chaine de caractère ");
            str = Console.ReadLine();
            Console.WriteLine(Inverser(str));
            Console.WriteLine(Palindrome("Radar"));
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Acronyme("Organisation du traité de l’Atlantique Nord"));

            Console.ReadKey();
        }

        // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // exemple : bonjour => ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                sb.Append(str[str.Length - i - 1]);
            }
            return sb.ToString();
        }

        // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est un palindrome
        // Ex: SOS, radar
        static bool Palindrome(string str)
        {
            string tmp = str.Trim().ToLower();
            return tmp == Inverser(tmp);
        }

        //Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
        //Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
        //Ex:   Comité international olympique → CIO
        //      Organisation du traité de l’Atlantique Nord → OTAN
        static String Acronyme(string str)
        {
            string acr = "";
            string[] tabStr = str.Trim().ToUpper().Replace('’', ' ').Split();
            foreach (string s in tabStr)
            {
                if (s.Length > 2)
                {
                    acr += s[0];
                }
            }
            return acr;
        }

    }
}
