﻿using System;

namespace _12_ClasseBase
{
    class Calcul<T>
    {
        T a;
        T b;

        public Calcul(T a, T b)
        {
            this.a = a;
            this.b = b;
        }

        public void Afficher()
        {
            Console.WriteLine(a + " " + b);
        }

        public void Test<U>(U a)
        {
            Console.WriteLine(a);
        }
    }
}
