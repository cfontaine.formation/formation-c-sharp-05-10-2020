﻿using System;

namespace _03_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Condition if
            int v = 12;
            if (v > 34)
            {
                Console.WriteLine("i est supérieur à 34");
            }
            else if (v > 100)
            {
                Console.WriteLine("i est pas supérieur à 100");
            }
            else if (v < 20)
            {
                Console.WriteLine("i est pas inférieur à 20");
            }

            // Exercice: Valeur absolue
            // Saisir 1 chiffres et afficher la valeur absolue sous la forme | -1.85 |= 1.85
            double val = Convert.ToDouble(Console.ReadLine());
            if (val < 0)
            {
                Console.WriteLine("|{0}|={1}", val, -val);
            }
            else
            {
                Console.WriteLine("|{0}|={1}", val, val);
            }

            // Exercice: Parité
            // Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
            int par = Convert.ToInt32(Console.ReadLine());
            if (par % 2 == 0)
            {
                Console.WriteLine("Le nombre est paire");
            }
            else
            {
                Console.WriteLine("Le nombre est impaire");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclus) et 7(inclus)
            int inter = Convert.ToInt32(Console.ReadLine());
            if (inter > -4 && inter <= 7)
            {
                Console.WriteLine("{0} fait partie de l'intervalle", inter);
            }

            // Condition switch
            int jours = 7;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un double v1
            // - une chaine de caractère opérateur qui a pour valeur valide : +-* /
            // - un double v2
            // Afficher:
            //  Le résultat de l’opération
            //  Une message d’erreur si l’opérateur est incorrecte
            //  Une message d’erreur si l’on fait une division par 0
            double va = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double vb = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine("{0} + {1}= {2}", va, vb, va + vb);
                    break;
                case "-":
                    Console.WriteLine("{0} - {1}= {2}", va, vb, va - vb);
                    break;
                case "*":
                    Console.WriteLine("{0} * {1}= {2}", va, vb, va * vb);
                    break;
                case "/":
                    if (vb == 0.0) // vb>-0.00000001 && vb <0.00000001
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine("{0} / {1}= {2}", va, vb, va / vb);
                    }
                    break;
                default:
                    Console.WriteLine("L'opérateur {0} n'est pas valide", op);
                    break;
            }

            // Opérateur ternaire utilisation => affectation conditionnelle
            par = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(par % 2 == 0 ? "Le nombre est paire" : "Le nombre est impaire");

            // Instructions de branchement
            // break
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i={0}", i);
                if (i == 3)
                {
                    break;  // break => termine la boucle
                }
            }

            // continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine("i={0}", i);
            }

            // goto
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.WriteLine("i={0} j={1}", i, j);
                    if (i == 3)
                    {
                        goto EXIT_LOOP;     // Utilisation de goto pour sortir de boucles imbriquées
                    }
                }
            }
        EXIT_LOOP:

            jours = 1;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto case 6;            // Utilisation de goto pour se transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch
                case 2:
                    Console.WriteLine("Mardi");
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice Table Multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
            //   1 X 4 = 4
            //   2 X 4 = 8
            //   …
            //   9 x 4 = 36
            // Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
            for (; ; ) // ou while(true) boucle inifinie
            {
                int valM = Convert.ToInt32(Console.ReadLine());
                if (valM < 1 || valM > 9)
                {
                    break;
                }
                for (int i = 1; i <= 9; i++)
                {
                    Console.WriteLine("{0} x {1} = {2}", i, valM, i * valM);
                }
            }

            // Quadrillage
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne

            // ex: pour 2 3
            //[ ][ ]
            //[ ][ ]
            //[ ][ ]
            Console.WriteLine("Entrer le nombre de colonne= ");
            int nbColonne = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Entrer le nombre de ligne= ");
            int nbLigne = Convert.ToInt32(Console.ReadLine());

            for (int l = 0; l < nbLigne; l++)
            {
                for (int c = 0; c < nbColonne; c++)
                {
                    Console.Write("[ ] ");
                }
                Console.WriteLine("");
            }

            Console.ReadKey();
        }
    }
}
