﻿using System;

namespace _05_methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(3, 8));

            // Appel de methode (sans retour)
            Afficher(23);

            // Exercice méthode parité
            Console.WriteLine(Even(2));
            Console.WriteLine(Even(3));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int x = 2;
            int y = 3;
            PermuterValeurImpossible(x, y);
            Console.WriteLine("para valeur x={0} et y={1}", x, y);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            PermuterReference(ref x, ref y);
            Console.WriteLine("par référence x={0} et y={1}", x, y);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée


            // Il faut utiliser le mot clé out pour la définition de méthode mais aussi lors de l'appel
            // On peut de déclarer la variable de retour dans les arguments pendant l'appel de la fonction
            // int s1;
            MethodeParamSortie(2, out _, out int s1);  // On peut ignorer un paramètre out en le nommant _ 
            Console.WriteLine(s1);

            // Paramètre optionnel
            MethodeParamOptionnel(12, "hello", 3.4, true);
            MethodeParamOptionnel(12, "hello");
            MethodeParamOptionnel(12, "hello", 34.0);

            // Paramètres nommés
            MethodeParamOptionnel(tst: true, val: 12, d: 12.67, str: "world");
            MethodeParamOptionnel(tst: true, val: 12, str: "world");

            // Nombre de pramètre variable
            Console.WriteLine(Moyenne(1, 4, 5));
            Console.WriteLine(Moyenne(1, 4, 5, 7, 7, 9, 3));
            Console.WriteLine(Moyenne());

            // Surcharge de méthode
            Console.WriteLine(Multiplication(1, 2));
            Console.WriteLine(Multiplication(4.9, 5.9));
            Console.WriteLine(Multiplication(4.9, 1));
            Console.WriteLine(Multiplication(4, 1.7)); // conversion implicite de 4 (entier en double) 4.0

            // Méthode récursive
            Console.WriteLine(Factorial(3));

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (string str in args)
            {
                Console.WriteLine(str);
            }

            // Exercice: Methode Tableau
            Menu();
            Console.ReadKey();
        }

        static int Somme(int va, int vb)
        {
            return va + vb; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur (expression à droite)
        }
        static void Afficher(int v)  // void => pas de valeur retournée
        {
            Console.WriteLine(v);
            //   avec void => return; ou return peut être omis 
        }

        #region Passage de paramètre
        // Passage par valeur
        static void PermuterValeurImpossible(int a, int b)
        {
            int tmp = a;
            a = b;
            b = tmp;     // La modification  des valeurs des paramètres a et b n'a pas d'influence en dehors de la méthode
            Console.WriteLine("a={0} et b={1}", a, b);
        }

        // Passage par référence => ref
        static void PermuterReference(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
            Console.WriteLine("a={0} et b={1}", a, b);
        }

        // Passage de papramètre en sortir => out
        static void MethodeParamSortie(int val, out int valOut1, out int valOut2)
        {
            valOut1 = val * 2;      // La méthode doit obligatoirement affecter une valeur aux paramètres out
            valOut2 = val + 20;
        }

        // Pour les arguments passés par valeur, on peut leurs donner des valeurs par défaut
        static void MethodeParamOptionnel(int val, string str, double d = 10.0, bool tst = false)
        {
            Console.WriteLine("val={0}, str={1}, d={2} et tst={3} ", val, str, d, tst);
        }

        // Nombre d'arguments variable => params
        static double Moyenne(params int[] valeurs)
        {
            if (valeurs.Length == 0)
            {
                return 0.0;
            }
            double somme = 0;
            foreach (int v in valeurs)
            {
                somme += v;
            }
            return somme / valeurs.Length;
        }
        #endregion

        #region Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Multiplication(int a, int b)
        {
            Console.WriteLine("2 int");
            return a * b;
        }

        static double Multiplication(double a, double b)
        {
            Console.WriteLine("2 double");
            return a * b;
        }

        static double Multiplication(double a, int b)
        {
            Console.WriteLine("1 int, 1 double");
            return a * b;
        }
        #endregion

        #region Recursivite
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion

        #region Exercice_Parite
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Even(int p)
        {
            return p % 2 == 0;

            // ou
            //return p % 2 == 0? true:false;

            // ou
            //if (p % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        #endregion

        #region Exercice_Methode_Tableau
        // - Écrire un méthode qui affiche un tableau d’entier
        // - Écrire une méthode qui permet de saisir :
        //      - La taille du tableau
        //      - Les éléments du tableau
        // - Écrire une méthode qui calcule :
        //      - le minimum d’un tableau d’entier
        //      - le maximum
        //      - la moyenne
        // - Faire un menu qui permet de lancer ces méthode
        static void AfficherTableau(int[] tab)
        {
            Console.Write("[ ");
            foreach (int v in tab)
            {
                Console.Write("{0} ", v);
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTableau()
        {
            int size;
            do
            {
                Console.Write("Saisir la taille du tableau ");
                size = Convert.ToInt32(Console.ReadLine());
            } while (size <= 0);

            int[] t = new int[size];
            for (int i = 0; i < t.Length; i++)
            {
                Console.Write("t[{0}]=", i);
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            return t;
        }

        static void CalculerTableau(int[] tab, out int min, out int max, out double moyenne)
        {
            min = int.MaxValue;
            max = int.MinValue;
            double somme = 0.0;
            foreach (int v in tab)
            {
                if (v < min)
                {
                    min = v;
                }
                if (v > max)
                {
                    max = v;
                }
                somme += v;
            }
            moyenne = somme / tab.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("\n________________________________________________________");
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afifcher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
            Console.WriteLine("________________________________________________________");
            Console.Write("Choix= ");
        }

        static void Menu()
        {
            int[] tab = null;
            int choix;
            do
            {
                AfficherMenu();
                choix = Convert.ToInt32(Console.ReadLine());
                if ((choix == 2 || choix == 3) && tab == null)
                {
                    Console.WriteLine("Le tableau n'a pas été saisie");
                }
                else
                {
                    switch (choix)
                    {
                        case 1:
                            tab = SaisirTableau();
                            break;
                        case 2:
                            AfficherTableau(tab);
                            break;
                        case 3:
                            CalculerTableau(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine("Maximum={0} , Minimum={1}, Moyenne={2}", minimum, maximum, moyenne);
                            break;
                    }
                }
            }
            while (choix != 0);
        }
        #endregion
    }
}
