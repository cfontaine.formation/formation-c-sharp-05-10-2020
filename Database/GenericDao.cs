﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Database
{
    // Dao Générique 
    // Dao =>   un objet qui va permettre de persister un objet en base de donnée
    //          se sera le seul objet qui va contenir les requêtes SQL
    //          on aura un objet Dao pour chaque classe a persiter
    // La classe Dao générique est un classe abstraite qui va contenir et gérer la connection à la base de donnée
    // dans les classes filles on redéfinira les méthodes abstraites qui contiendront les requetes associées a chaque objet
    public abstract class GenericDao<T> where T : DbObject // Le type générique T ne pourra qu'être une sous classe de DBobject
    {

        public static string ConnectionStr { private get; set; }

        protected static MySqlConnection cnx;

        public void SaveOrUpdate(T elm, bool close = true) // si close est à false la connection à la base n'est pas fermer, elle pourra être utilisé pour une autre requête 
        {
            if (elm.Id == 0)
            {
                Create(GetConnection(), elm);
            }
            else
            {
                Update(GetConnection(), elm);
            }
            CloseConnection(close);
        }

        public void Delete(T elm, bool close = true)
        {
            Delete(GetConnection(), elm);
            CloseConnection(close);
        }

        public T Read(long id, bool close = true)
        {
            T elm = Read(GetConnection(), id);
            CloseConnection(close);
            return elm;
        }

        public List<T> ReadAll(bool close = true)
        {
            List<T> lst = ReadAll(GetConnection());
            CloseConnection(close);
            return lst;
        }

        protected MySqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new MySqlConnection(ConnectionStr); // créer la connection avec la base de donnée
                cnx.Open();
            }
            return cnx;
        }

        protected void CloseConnection(bool close)
        {
            if (close)
            {
                cnx.Close();        // fermer la connection avec la base de donnée
                cnx = null;
            }
        }

        protected abstract void Create(MySqlConnection cnx, T elm);
        protected abstract void Update(MySqlConnection cnx, T elm);

        protected abstract void Delete(MySqlConnection cnx, T elm);

        protected abstract T Read(MySqlConnection cnx, long id);

        protected abstract List<T> ReadAll(MySqlConnection cnx);
    }
}
