﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Database
{
    // Dao pour persister les objets contact, on redéfinit les méthodes abstraite qui vont contenir les requête SQL
    public class ContactDao : GenericDao<Contact>
    {
        protected override void Create(MySqlConnection cnx, Contact elm)
        {
            string req = "INSERT INTO t_contact(prenom,nom,date_naissance,email) VALUES(@prenom,@nom,@dateNaissance,@email)";
            MySqlCommand cmd = new MySqlCommand(req, cnx);       // MySqlCommand objet qui va contenir la commande sql
            cmd.Parameters.AddWithValue("@prenom", elm.Prenom); // Pour remplacer les paramètres par les valeurs dans la requète
            cmd.Parameters.AddWithValue("@nom", elm.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", elm.JourNaissance);
            cmd.Parameters.AddWithValue("@email", elm.Email);
            cmd.ExecuteNonQuery();  // pour executer la requete SQL pour des commandes qui ne retourne pas de données (INSERT,DLETE,UPDATE)
            elm.Id = cmd.LastInsertedId; // pour récupérer la clé primaire générée par la base de donnée 
        }

        protected override void Delete(MySqlConnection cnx, Contact elm)
        {
            string req = "DELETE FROM t_contact WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", elm.Id);
            cmd.ExecuteNonQuery();
        }

        protected override Contact Read(MySqlConnection cnx, long id)
        {
            Contact contact = null;
            string req = "SELECT id,prenom,nom,date_naissance,email FROM t_contact  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader(); // pour éxecuter une requête SQL qui retourne des données (SELECT) les données retournées sont stockées dans un objet MySqlDataReader
            if (reader.Read()) // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat 
            {
                contact = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("date_naissance"), reader.GetString("email"));
                contact.Id = reader.GetInt64("id");
            }
            return contact;
        }

        protected override List<Contact> ReadAll(MySqlConnection cnx)
        {
            List<Contact> lst = new List<Contact>();
            string req = "SELECT id,prenom,nom,date_naissance,email FROM t_contact";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Contact c = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("date_naissance"), reader.GetString("email"));
                c.Id = reader.GetInt64("id");
                lst.Add(c);
            }
            return lst;
        }

        protected override void Update(MySqlConnection cnx, Contact elm)
        {
            string req = "UPDATE t_contact SET nom = @nom, prenom=@prenom, date_naissance=@dateNaissance, email=@email  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", elm.Id);
            cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
            cmd.Parameters.AddWithValue("@nom", elm.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", elm.JourNaissance);
            cmd.Parameters.AddWithValue("@email", elm.Email);
            cmd.ExecuteNonQuery();
        }
    }
}
