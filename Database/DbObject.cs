﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database
{   // Dbobject classe abstraite de base pour tous les objets qui vont être persistés en base de donnée
    // Elle permet de gérer la clé primaire pour les objets persistés
    public abstract class DbObject
    {
        public long Id { get; set; }

        public override bool Equals(object obj)     // l'égalité de objet se fera par rapport à la clef primaire
        {
            return obj is DbObject @object &&
                   Id == @object.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}", Id);
        }
    }
}
