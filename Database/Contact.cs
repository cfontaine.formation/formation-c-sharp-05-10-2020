﻿using System;

namespace Database
{
    public class Contact : DbObject
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public DateTime JourNaissance { get; set; }
        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime jourNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            JourNaissance = jourNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return string.Format(" Contact[{0} {1} {2} {3} {4} ", Prenom, Nom, JourNaissance.ToString(), Email, base.ToString());
        }
    }
}
