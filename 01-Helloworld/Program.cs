﻿using System;

// Commmentaire fin de ligne

/*
 * Commentaire sur plusieurs 
 * Lignes
 */

//TODO Finir le programme
namespace _01_Helloworld
{
    /// <summary>
    /// Classe principale
    /// </summary>
    class Program
    {
        /// <summary>
        /// Méthode principale point d'entrée du programme
        /// </summary>
        /// <param name="args">Les arguments de la ligne de commande</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            // Pour maintenir la console affichée
            Console.ReadKey();  // Lecture d'une touche au clavier
        }
    }
}
