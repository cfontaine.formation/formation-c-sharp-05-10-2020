﻿using System;

namespace _07_Poo
{
    public class Adresse
    {
        public string Rue { get; set; }
        public string Ville { get; set; }
        public int CodePostal { get; set; }

        public Adresse(string rue, string ville, int codePostal)
        {
            Rue = rue;
            Ville = ville;
            CodePostal = codePostal;
        }

        public void Afficher()
        {
            Console.WriteLine("{0} {1} ({2})", Rue, Ville, CodePostal);
        }
    }
}
