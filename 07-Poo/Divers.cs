﻿using System;

namespace _07_Poo
{
    class Divers
    {
        public int Data { get; set; } = 42;

        // en C#6, une méthode qui ne contient qu'un seule instruction peut s'écrire avec l'opérateur => 
        // valable pour le méthodes les constructeurs et le destructeur
        public Divers(int data) => Data = data; // Un Constructeur avec une ligne de code

        public void Afficher() => Console.WriteLine(Data); // Une méthode avec une seule ligne de code (sans type de retour)

        public int Calcul(int mul) => mul * Data; // Si la méthode retourne une valeur, il n'y a pas de return


        // Méthode locale en C#7 => une méthode qui est déclaré dans une méthode
        // Elle n'est accéssible que dans la méthode où elle est déclarée
        public void TestMethodeLocale()
        {
            int valeur = 34;
            Console.WriteLine(mult2());
            // Console.WriteLine(valeurInterne); // On ne peut pas accéder au variable de la fonction locale en dehors de celle-ci

            int mult2() // une méthode locale 
            {
                int valeurInterne = 23;
                return valeur * 2;  // on peut accéder a une variable de la méthode "extérieur" dans la fonction locale
            }
        }

        // Type annonyme 
        public void TestTypeAnonyme()
        {
            var ty = new { var1 = 12, message = "Bonjour" };  // => déclaration d'un type anonyme
            Console.WriteLine(ty.var1);
            Console.WriteLine(ty.message);
        }
    }
}
