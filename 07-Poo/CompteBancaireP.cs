﻿using System;

namespace _07_Poo
{
    // Exercice compte bancaire avec agrégation
    class CompteBancaireP
    {

        public double Solde { get; private set; } = 50.0;
        public string Iban { get; }
        public Personne Titulaire { get; }

        public static int cptCompte;

        public CompteBancaireP(Personne titulaire)
        {
            cptCompte++;
            Iban = "fr-5962-0000-" + cptCompte;
            Titulaire = titulaire;
        }

        public CompteBancaireP(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine("Solde={0}", Solde);
            Console.WriteLine("Iban={0}", Iban);
            Console.Write("Titulaire=");
            Titulaire.Afficher();
            Console.WriteLine("_________________________________");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0.0;
        }
    }
}

