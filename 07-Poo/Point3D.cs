﻿using System;

namespace _07_Poo
{
// Exercice Indexeur
// Créer une classe Point3D
// Propriétés
// - trois coordonnée X, Y, Z
// Méthode
// - afficher => affiche les coordonnée sous la forme(x, y, z)
// un indexeur qui va prendre en paramètre 1 -> x , 2->y, 3->z
// si on est en dehors des valeurs autorisées 1,2,3 on génère une erreur avec : throw new IndexOutOfRangeException();
// Point3D p = new Point3D(1, 4, 7);
// p.X=2;
// p[1]=2; // affecter la propriété X
    class Point3D
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public Point3D(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public void Afficher()
        {
            Console.WriteLine("({0},{1},{2})", X, Y, Z);
        }
        public int this[int index]
        {
            get
            {
                switch (index)
                {
                    case 1:
                        return X;
                    case 2:
                        return Y;
                    case 3:
                        return Z;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
            set
            {
                switch (index)
                {
                    case 1:
                        X = value;
                        break;
                    case 2:
                        Y = value;
                        break;
                    case 3:
                        Z = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }

        }
    }
}
