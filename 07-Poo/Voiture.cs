﻿using System;

namespace _07_Poo
{
    public class Voiture
    {
        // Variables d'instances => Etat
        string marque = "Honda";
        //string couleur = "Noir";  // On n'a plus besoin de déclarer les variables d'instances, elles seront générées automatiquement 
        //string plaqueIma;         // par les propriétées auto-implémentés
        //int vitesse;
        private double niveauCarburant = 10;

        // Variable de classe
        // public static int cptVoiture; // Remplacer par une propriétée static

        // Propriétés
        public double NiveauCarburant
        {
            get
            {
                return niveauCarburant;
            }
            set
            {
                if (value >= 0)
                {
                    niveauCarburant = value;
                }
            }
        }

        // C# 7.0
        public string Marque
        {
            get => marque;
            set => marque = value;
        }

        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Couleur { get; set; } = "Noir";   // On peut donner un valeur par défaut à une propriétée ( littéral, expression ou une fonction)
        public string PlaqueIma { get; private set; }   // On peut associé un modificateur d'accès à get et à set (doit être plus restritif que le modificateur de la propriété) 
        public int Vitesse { get; private set; }
        public static int CompteurVoiture { get; private set; }  // Une propriété peut-être static
        public Personne Proprietaire { get; set; }

        // Constructeurs => On peut surcharger le constructeur
        public Voiture()    // Constructeur par défaut
        {
            CompteurVoiture++;
            Console.WriteLine("Constructeur par défaut");
        }
        public Voiture(string marque, string plaqueIma) : this() // Chainnage de constructeur : appel du constructeur par défaut
        {
            Console.WriteLine("2 paramètres");
            this.marque = marque;
            PlaqueIma = plaqueIma;
        }

        public Voiture(string marque, string couleur, string plaqueIma) : this(marque, plaqueIma) // Chainnage de constructeur : appel du constructeur par défaut Voiture(string marque, string plaqueIma)
        {
            Console.WriteLine("3 paramètres");
            Couleur = couleur;
        }

        public Voiture(string marque, string couleur, string plaqueIma, Personne proprietaire) : this(marque, couleur, plaqueIma) // Chainnage de constructeur : appel du constructeur par défaut Voiture(string marque, string plaqueIma)
        {
            Proprietaire = proprietaire;
        }
        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static");
        }

        // Destructeur pour libérer des ressources 
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur");
        //}


        // Méthodes d'instances => comportement
        public void Accelerer(int accVitesse)
        {
            if (accVitesse > 0)
            {
                Vitesse += accVitesse;
            }
        }
        public void Freiner(int frnVitesse)
        {
            if (frnVitesse > 0)
            {
                Vitesse -= frnVitesse;
            }
            if (Vitesse < 0)
            {
                Vitesse = 0;
            }
        }
        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public void Afficher()
        {
            Console.WriteLine("Marque={0} Couleur={1} Plaque={2} Vitesse={3} Caburant={4}", marque, Couleur, PlaqueIma, Vitesse, niveauCarburant);
            if (Proprietaire != null)
            {
                Proprietaire.Afficher();
            }
        }

        public bool TestEgalitePlaque(Voiture a)
        {
            return PlaqueIma == a.PlaqueIma;
        }

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            // vitesse = 0;  // Dans une méthode de classe ont n'a pas accès à une variable d'instance    
            Console.WriteLine(CompteurVoiture); // On peut accéder dans une méthode de classe à une variable de classe 
        }

        public static bool TestEgalitePlaque(Voiture a, Voiture b)
        {
            return a.PlaqueIma == b.PlaqueIma; // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
