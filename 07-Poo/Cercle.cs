﻿namespace _07_Poo
{
    // Exercice Cercle
    //    Écrire une classe Cercle
    // Propriété :
    //  - centre : un point
    //  - rayon par défaut le rayon est égal à 1
    // Constructeurs
    //  - Un constructeur par défaut
    //  - Un constructeur qui permet d’initialiser le cercle
    // Méthodes
    // - Une méthode qui permet de tester la collision entre 2 cercles :
    //   s'il y a collision-> La distance entre les centres des 2 cercles doit être inférieure ou égal à la somme des rayon
    // - Une méthode qui permet de tester si un point est contenu dans le cercle :
    //   la distance entre le centre du cercle et le point est inférieur ou égale au rayon du cercle
    class Cercle
    {
        public Point Centre { get; set; }
        public double Rayon { get; set; } = 1.0;

        public Cercle()
        {

        }
        public Cercle(Point centre, double rayon)
        {
            Centre = centre;
            Rayon = rayon;
        }

        public bool Collision(Cercle c)
        {
            return Point.Distance(Centre, c.Centre) <= (Rayon + c.Rayon);
        }

        public bool Inclusion(Point p)
        {
            return Point.Distance(Centre, p) <= Rayon;
        }
    }
}
