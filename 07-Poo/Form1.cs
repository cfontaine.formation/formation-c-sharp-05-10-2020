﻿namespace _07_Poo
{
    // Classe partielle répartie sur plusieurs fichiers
    partial class Form1
    {

        public int Data { get; set; }

        public Form1(int data)
        {
            Data = data;
        }
    }
}
