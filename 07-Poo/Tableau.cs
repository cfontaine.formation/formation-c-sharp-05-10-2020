﻿using System;

namespace _07_Poo
{
    class Tableau
    {
        int[] data;

        public Tableau(int size)
        {
            data = new int[size];
        }

        public void Resize(int size)
        {
            int[] tmp = new int[size];
            int limit = size > data.Length ? data.Length : size;
            for (int i = 0; i < limit; i++)
            {
                tmp[i] = data[i];
            }
            data = tmp;
        }

        public int Size()
        {
            return data.Length;
        }

        // Indexeur
        public int this[string strIndex]
        {
            get
            {
                int index = Convert.ToInt32(strIndex);
                if (index >= 0 && index < data.Length)
                {
                    return data[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                int index = Convert.ToInt32(strIndex);
                if (index >= 0 && index < data.Length)
                {
                    data[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        public int this[int index]
        {
            get
            {
                if (index >= 0 && index < data.Length)
                {
                    return data[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (index >= 0 && index < data.Length)
                {
                    data[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
    }
}
