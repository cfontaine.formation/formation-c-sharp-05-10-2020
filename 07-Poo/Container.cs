﻿using System;

namespace _07_Poo
{
    // Classe imbriquée
    public class Container
    {

        private static string varClasse = "Variable de classe";

        private string varInstance = "Variable d'instance";

        public void TestContainer()
        {
            Element elm = new Element();
            elm.TestElementVarClasse();
            elm.TestElementVarInstance(this);
        }

        class Element // par défaut private
        {
            public void TestElementVarClasse()
            {
                Console.WriteLine(varClasse); // On a accès au variable de classe de extérieur
            }

            public void TestElementVarInstance(Container c)
            {
                Console.WriteLine(c.varInstance);
            }

        }
    }
}
