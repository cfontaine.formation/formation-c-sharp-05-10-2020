﻿using System;

namespace _07_Poo
{
    // Classe partielle répartie sur plusieurs fichiers
    partial class Form1
    {
        public void Afficher()
        {
            Console.WriteLine(Data);
        }
    }
}