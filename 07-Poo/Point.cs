﻿using System;

namespace _07_Poo
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Afficher()
        {
            Console.WriteLine("({0},{1})", X, Y);
        }

        public void Deplacer(int tx, int ty)
        {
            X += tx;
            Y += ty;
        }

        public double Norme()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        public static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

    }
}
