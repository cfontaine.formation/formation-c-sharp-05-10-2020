﻿using System;

namespace _07_Poo
{
    public class CompteBancaire
    {
        public double Solde { get; private set; } = 50.0;
        public string Iban { get; }
        public string Titulaire { get; }

        public static int cptCompte;

        public CompteBancaire(string titulaire)
        {
            cptCompte++;
            Iban = "fr-5962-0000-" + cptCompte;
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, string titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine("Solde={0}", Solde);
            Console.WriteLine("Iban={0}", Iban);
            Console.WriteLine("Titulaire={0}", Titulaire);
            Console.WriteLine("_________________________________");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0.0;
        }
    }

}
