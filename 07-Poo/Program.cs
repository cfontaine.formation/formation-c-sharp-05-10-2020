﻿using System;

namespace _07_Poo
{
    class Program
    {
        static void Main()
        {
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine("Compteur voiture={0}", Voiture.CompteurVoiture);

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();

            Console.WriteLine("Compteur voiture={0}", Voiture.CompteurVoiture);
            // Appel d’une méthode d’instance
            v1.Afficher();

            // Accès à une propriété en ecriture (set)
            v1.Marque = "Fiat";
            v1.Couleur = "Rouge";
            // v1.PlaqueIma = "fr59-0000";   // On ne peut pas accéder aux propriétés en écriture (pas de set)
            // v1.Vitesse = 10;

            // Accès à une propriété en lecture (get)
            v1.NiveauCarburant = 30;// v1.gaugeCarburant = 30; // On ne peut plus accéder directement à la variable d'instance, on n'y accède par l'intermédiare d'une propriété
            v1.Afficher();

            Console.WriteLine(v1.Marque);
            v1.Accelerer(20);
            Console.WriteLine(v1.Vitesse);
            v1.Freiner(5);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());
            Console.WriteLine(v1.Vitesse);

            Voiture v2 = new Voiture("Opel", "Gris", "FR59-001");
            Console.WriteLine("Compteur voiture={0}", Voiture.CompteurVoiture);
            v2.Afficher();
            v2.Accelerer(30);
            v2.Afficher();

            // Test de l'appel du destructeur
            //v1 = null;    // En affectant, les références v1 et v2 à null. Il n' y a plus de référence sur les objet voitures   
            //v2 = null;    // Ils seront détruits lors du prochain appel du destructeur
            //GC.Collect(); // Appel explicite au garbage Collector, les destructeurs des 2 objets sont appelés

            Console.WriteLine("test égalité voiture {0}", Voiture.TestEgalitePlaque(v1, v2));
            Voiture v3 = new Voiture { Couleur = "Jaune", Marque = "Toyota" };//, PlaqueIma = "fr59-3448" };
            Console.WriteLine("Compteur voiture={0}", Voiture.CompteurVoiture);
            v3.Afficher();
            Personne per1 = new Personne("John", "Doe", "jDoe@dawan.fr");
            v3.Proprietaire = per1;
            v3.Afficher();

            // Indexeur
            Tableau tab = new Tableau(3);
            tab["0"] = 1;
            tab["1"] = 2;
            tab["2"] = 3;
            tab.Resize(5);
            tab[3] = 4;
            tab["4"] = 5;
            for (int i = 0; i < tab.Size(); i++)
            {
                Console.WriteLine(tab[i]);
            }

            // Classe Imbriquée
            Container c = new Container();
            c.TestContainer();

            // Si la classe imbriquée est public ou internal, on peut l'instancié en dehors du conteneur
            //Container.Element elm = new Container.Element(); 
            //elm.TestElementVarClasse();
            //elm.TestElementVarInstance(c);


            // Classe Partielle => classe définit sur plusieurs fichiers
            Form1 form1 = new Form1(42);
            form1.Afficher();

            // Classe Static => ne contient que des méthodes et des variables de classe 
            string str = "azerty";// Console.ReadLine();
            if (Tools.CheckNotEmpty(str) && Tools.CheckMinSize(str, 5))
            {
                Console.WriteLine("str={0}", str);
            }
            Console.WriteLine(Tools.Message);
            // Tools t = new Tools(); // Impossible on ne peut pas instancié une classse static

            Divers divers = new Divers(10);
            Console.WriteLine(divers.Calcul(4));
            divers.Afficher();

            // Type annonyme
            divers.TestTypeAnonyme();

            // Méthode d'extension
            str.Hello("John");

            // Exercice CompteBancaire
            CompteBancaire cb = new CompteBancaire(100.0, "John Doe");
            //cb.Iban = "FR62-00000-00000";
            //cb.Solde = 100.0;
            //cb.Titulaire = "John Doe";
            cb.Afficher();
            cb.Crediter(1000.0);
            cb.Afficher();
            cb.Debiter(1500.0);
            cb.Afficher();
            Console.WriteLine(cb.EstPositif());

            CompteBancaire cb2 = new CompteBancaire(1500.0, "Alan Smithee");
            cb2.Afficher();

            CompteBancaire cb3 = new CompteBancaire(5000.0, "Jane Doe");
            cb3.Afficher();

            // Exercice Point 
            Point p1 = new Point(1, 2);
            //p1.X = 1;
            //p1.Y = 2;
            p1.Afficher();
            p1.Deplacer(5, 1);
            p1.Afficher();
            Console.WriteLine("Norme= {0}", p1.Norme());

            Point p2 = new Point(10, 2);
            Console.WriteLine("Distance= {0}", Point.Distance(p1, p2));

            // Exercice Indexeur Point3D
            Point3D pt3D = new Point3D(1, 4, 6);
            pt3D.Afficher();
            pt3D[1] = 4;
            pt3D.Afficher();
            pt3D.X = 100;
            pt3D.Afficher();

            // Exercice agrégation
            Cercle cercleA = new Cercle(new Point(1, 0), 2.0);
            Cercle cercleB = new Cercle(new Point(2, 0), 2.0);
            Point pt1 = new Point(1, 1);
            Console.WriteLine(cercleA.Collision(cercleB));
            Console.WriteLine(cercleA.Inclusion(pt1));

            Adresse adr1 = new Adresse("1,rue Esquermoise", "Lille", 59000);
            Personne per2 = new Personne("Alan", "Smithee", "as@dawan.fr", adr1);
            CompteBancaireP cp = new CompteBancaireP(100.0, per2);
            cp.Afficher();

            Console.ReadKey();
        }
    }
}
