﻿using System;

namespace _07_Poo
{
    public class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public string Email { get; set; }

        public Adresse Adresse { get; set; }

        public Personne(string prenom, string nom, string email)
        {
            Prenom = prenom;
            Nom = nom;
            Email = email;
        }

        public Personne(string prenom, string nom, string email, Adresse adresse) : this(prenom, nom, email)
        {
            Adresse = adresse;
        }

        public void Afficher()
        {
            Console.WriteLine("{0} {1} ({2})", Prenom, Nom, Email);
            if (Adresse != null)
            {
                Adresse.Afficher();
            }
        }
    }
}
