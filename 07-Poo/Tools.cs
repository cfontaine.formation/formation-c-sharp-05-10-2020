﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace _07_Poo
{
    // Classe static 
    // - Elle ne peut pas être instanciée
    // - Elle contient uniquement des membres statiques
    // - Elle n'a pas de constructeur
    // - On ne peut hériter de cette classe (sealed) 
    static class Tools
    {
        public const string Message = "Un message"; // par défaut const est static

        public static bool CheckNotNull(string str)
        {
            return str != null;
        }

        public static bool CheckNotEmpty(string str)
        {
            return str != null && str.Length>0;
        }

        public static bool CheckMinSize(string str, int minSize)
        {
            return str.Length > minSize;
        }

    }
}
