﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _07_Poo
{
    // Une méthode d'extension permet d'ajouter une méthode à une classe sans avoir accès à son code  
    // Elle doit se trouver dans une classe statique 
    static class MethodeExtension
    {
        // Une méthode d'extension doit ête public et statique
        // Le premier paramètre doit être la classe où l'on va ajouter la méthode précédé de this
        public static void Hello(this string str,string nom)
        {
            Console.WriteLine("Hello,{0}",nom);
        }
    }
}
