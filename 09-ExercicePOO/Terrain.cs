﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    class Terrain
    {
        Forme[] formes;

       public int NombreForme { get; private set; }

        public Terrain(int size=10)
        {
            formes = new Forme[size];
        }
        
        public void Ajouter(Forme forme)
        {
            if (NombreForme < formes.Length)
            {
                formes[NombreForme] = forme;
                NombreForme++;
            }
        }

        public double SurfaceTotal()
        {
            double surfaces = 0;
            for(int i=0;i< NombreForme;i++)
            {
                surfaces += formes[i].CalculSurface();
            }
            return surfaces;
        }

        public double SufaceTerrain(Couleur couleur)
        {
            double surfaces = 0;
            for (int i = 0; i < NombreForme; i++)
                if (formes[i].Couleur == couleur)
                {
                    {
                        surfaces += formes[i].CalculSurface();
                    }
            }
            return surfaces;
        }
    }
}
