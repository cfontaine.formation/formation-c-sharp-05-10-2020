﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    enum Couleur { Vert,Rouge,Bleu, Orange}
    abstract class Forme
    {
        public Couleur Couleur { get; protected set; }

        protected Forme(Couleur couleur)
        {
            Couleur = couleur;
        }

        public abstract double CalculSurface();
    }
}
