﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    class Rectangle : Forme
    {
        public double Largeur { get; set; }
        public double Longueur { get; set; }

        public Rectangle(double largeur,double longueur,Couleur couleur) : base(couleur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }

        public override double CalculSurface()
        {
            return Largeur * Longueur;
        }

        public override string ToString()
        {
            return string.Format("Rectangle[largeur={0}, longueur={1}, couleur={2}]", Largeur,Longueur, Couleur);
        }
    }
}
