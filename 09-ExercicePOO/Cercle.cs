﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    class Cercle : Forme
    {
        public double Rayon { get; set; }

        public Cercle(double rayon, Couleur couleur) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double CalculSurface()
        {
            return Math.PI * Math.Pow(Rayon,2);
        }

        public override string ToString()
        {
            return string.Format("Cercle[rayon={0}, Couleur={1}]", Rayon, Couleur);
        }
    }
}
