﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    class Program
    {
        static void Main(string[] args)
        {
            Terrain terrain = new Terrain();
            terrain.Ajouter(new Rectangle(1.0, 1.0, Couleur.Bleu));
            terrain.Ajouter(new Rectangle(1.0, 1.0, Couleur.Bleu));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleur.Vert));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleur.Vert));
            terrain.Ajouter(new Cercle(1.0, Couleur.Rouge));
            Console.WriteLine(terrain.SurfaceTotal());
            Console.WriteLine(terrain.SufaceTerrain(Couleur.Bleu));
            Console.WriteLine(terrain.SufaceTerrain(Couleur.Rouge));
            Console.WriteLine(terrain.SufaceTerrain(Couleur.Vert));
            Console.WriteLine(terrain.SufaceTerrain(Couleur.Orange));

            Console.ReadKey();
        }
    }
}
