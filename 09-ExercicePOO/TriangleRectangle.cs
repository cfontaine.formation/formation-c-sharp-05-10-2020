﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_ExercicePOO
{
    class TriangleRectangle : Forme
    {
        public double Largeur { get; set; }
        public double Longueur { get; set; }

        public TriangleRectangle(double largeur, double longueur,Couleur couleur): base(couleur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }

        public override double CalculSurface()
        {
            return Largeur * Longueur / 2;
        }

        public override string ToString()
        {
            return string.Format("Triangle rectangle[largeur={0}, longueur={1}, couleur={2}]", Largeur, Longueur, Couleur);
        }
    }
}
